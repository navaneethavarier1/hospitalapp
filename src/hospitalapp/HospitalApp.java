
import java.util.ArrayList;
import java.util.Scanner;

public class HospitalApp {

    static final int n = 3;

    public static User userDetails() {
        int userType = -1; // 1 - Old patient, 2 - New patient, 3 - Visitor
        int choice = -1;
        int patientType = -1;
        int searchType = -1;
        Scanner input = new Scanner(System.in);
        User user = new User();

        System.out.println("Welcome to PSIC. Please select one of the below options");
        System.out.println("Are you a visitor or a patient?");
        System.out.println("1. Visitor");
        System.out.println("2. Patient");
        userType = input.nextInt();

        if (userType == 1) {
            user.setTypeId(userType);
            searchPhy();
        } else if (userType == 2) {
            System.out.println("Are you a registered patient?");
            System.out.println("1. Registered patient");
            System.out.println("2. New patient");
            patientType = input.nextInt();

            if (patientType == 1) {

                System.out.println("Please select one of the following options:");
                System.out.println("1. Book an appointment");
                System.out.println("2. Cancel an appointment");
                System.out.println("3. View appointments");
                int option = input.nextInt();

                switch (option) {
                case 1:
                    searchPhy();
                    break;

                case 2:
                    System.out.println("Enter the slot ID you wish to cancel");
                    int id = input.nextInt();
                    MyDatabase db1 = new MyDatabase();

                    Timetable obj = new Timetable(db1.timetableDB.get(id).getSlotID(),
                            db1.timetableDB.get(id).getSlotType(), db1.timetableDB.get(id).getPhysicianName(),
                            db1.timetableDB.get(id).getTreatmentName(), db1.timetableDB.get(id).getTimeslot(),
                            db1.timetableDB.get(id).getRoomName(), "Cancelled");

                    db1.timetableDB.set(id, obj);
                    break;

                case 3:
                    System.out.println("Your appointments are :");
                    MyDatabase db = new MyDatabase();
                    db.addTimetable();
                    db.addAppointment();
                    for (int i = 0; i < db.appointmentDB.size(); i++) {
                        for (int j = 0; j < db.timetableDB.size(); j++) {
                            if (db.appointmentDB.get(i).getSlotID() == db.timetableDB.get(j).getSlotID()) {
                                System.out.println("Slot ID : " + db.timetableDB.get(j).getSlotID());
                                System.out.println("Treatment : " + db.timetableDB.get(j).getTreatmentName());
                                System.out.println("Physician : " + db.timetableDB.get(j).getPhysicianName());
                                System.out.println("Timeslot : " + db.timetableDB.get(j).getTimeslot());
                                System.out.println("Room : " + db.timetableDB.get(j).getRoomName());
                                System.out.println("*********************************************");
                            }
                        }
                    }
                    break;

                default:
                    System.out.println("Please enter a valid response");
                }

            } else if (patientType == 2) {
                System.out.println("Please enter the below details to register as a patient:-");
                System.out.println(patientDetailsInput().toString());
            }
            user.setTypeId(patientType);

        }

        return user;

    }

    public static void searchPhy() {
        MyDatabase db = new MyDatabase();
        Scanner input = new Scanner(System.in);

        System.out.println("Please select one of the below options to search for a physician");
        System.out.println("1. Name");
        System.out.println("2. Expertise");
        int choice = input.nextInt();

        db.addPhysician();
        db.addExpertise();
        db.addTimetable();
        switch (choice) {
        case 1:
            System.out.println("Choose one of the physicians:");
            for (int i = 0; i < 5; i++) {
                System.out.println((i + 1) + ". " + db.physiciansDB.get(i).getPhyName());
            }
            int searchType = input.nextInt();
            String phyName = db.physiciansDB.get(searchType - 1).getPhyName();
            System.out.println("Please select one of the available timeslots:-");
            for (int j = 0; j < 3; j++) {
                if ((db.timetableDB.get(j).getPhysicianName() == phyName)
                        && (db.timetableDB.get(j).getAppointmentStatus() == "Available")) {
                    System.out.println(j + ". " + "Treatment :" + db.timetableDB.get(j).getTreatmentName());
                    System.out.println("Timeslot :" + db.timetableDB.get(j).getTimeslot());
                    System.out.println("Room :" + db.timetableDB.get(j).getRoomName());
                    System.out.println("**************************************");

                }
            }

            int ch = input.nextInt();

            Timetable obj = new Timetable(db.timetableDB.get(ch).getSlotID(), db.timetableDB.get(ch).getSlotType(),
                    db.timetableDB.get(ch).getPhysicianName(), db.timetableDB.get(ch).getTreatmentName(),
                    db.timetableDB.get(ch).getTimeslot(), db.timetableDB.get(ch).getRoomName(), "Booked");

            db.timetableDB.set(ch, obj);
            break;

        case 2:
            System.out.println("Choose one of the area of expertise");
            for (int i = 0; i < 3; i++) {
                System.out.println((i + 1) + ". " + db.areaOfExpertise.get(i).getExp());
            }
            searchType = input.nextInt();

            String e = db.areaOfExpertise.get(searchType - 1).getExp();
            System.out.println("We have the below treatments available under this area of expertise.");
            System.out.println("Please select one of the available slots");

            for (int j = 0; j < 5; j++) {
                if (db.physiciansDB.get(j).getPhyAreaOfExpertise() == e) {
                    for (int k = 0; k < 3; k++) {
                        if (db.timetableDB.get(k).getPhysicianName() == db.physiciansDB.get(j).getPhyName()) {
                            System.out.println(
                                    (k + 1) + ". " + "Physician Name :" + db.timetableDB.get(k).getPhysicianName());
                            System.out.println("Treatment :" + db.timetableDB.get(k).getTreatmentName());
                            System.out.println("Timeslot :" + db.timetableDB.get(k).getTimeslot());
                            System.out.println("Room :" + db.timetableDB.get(k).getRoomName());
                            System.out.println("***********************************");
                        }
                    }
                }
            }

            break;

        default:
            System.out.println("Please enter a valid input");
        }
    }

    public static Patient patientDetailsInput() {
        String patientName = "";
        int telephone = -1;
        int uniqueId = -1;

        Scanner input = new Scanner(System.in);
        Patient patient = new Patient();

        do {
            System.out.println("Enter your Name: ");
            patientName = input.nextLine();
            System.out.println("Enter your Phone Number: ");
            telephone = input.nextInt();
            System.out.println("Enter your ID: ");
            uniqueId = input.nextInt();
        } while (patientName.isEmpty());
        patient.setName(patientName);
        patient.setPhoneNumber(telephone);
        patient.setId(uniqueId);
        return patient;
    }

    public static void main(String[] args) {

        System.out.println(userDetails());
        // System.out.println(searchPhyType());
        // System.out.println(patientDetailsInput().toString());
    }

}
