public class Appointment {
    private String userName;
    private int slotID;

    public Appointment(String userName, int slotID) {
        this.userName = userName;
        this.slotID = slotID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getSlotID() {
        return slotID;
    }

    public void setSlotID(int slotID) {
        this.slotID = slotID;
    }

    public Appointment() {
        this.userName = "";
        this.slotID = 0;
    }
}