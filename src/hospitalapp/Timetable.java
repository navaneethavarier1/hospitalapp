public class Timetable {
    private int slotID;
    private String slotType;
    private String physicianName;
    private String treatmentName;
    private String timeslot;
    private String roomName;
    private String appointmentStatus;

    public Timetable() {
        this.slotID = 0;
        this.slotType = "";
        this.physicianName = "";
        this.treatmentName = "";
        this.timeslot = "";
        this.roomName = "";
        this.appointmentStatus = "";
    }

    public Timetable(int slotID, String slotType, String physicianName, String treatmentName, String timeslot,
            String roomName, String appointmentStatus) {
        this.slotID = slotID;
        this.slotType = slotType;
        this.physicianName = physicianName;
        this.treatmentName = treatmentName;
        this.timeslot = timeslot;
        this.roomName = roomName;
        this.appointmentStatus = appointmentStatus;
    }

    public int getSlotID() {
        return slotID;
    }

    public void setSlotID(int slotID) {
        this.slotID = slotID;
    }

    public String getSlotType() {
        return slotType;
    }

    public void setSlotType(String slotType) {
        this.slotType = slotType;
    }

    public String getPhysicianName() {
        return physicianName;
    }

    public void setPhysicianName(String physicianName) {
        this.physicianName = physicianName;
    }

    public String getTreatmentName() {
        return treatmentName;
    }

    public void setTreatmentName(String treatmentName) {
        this.treatmentName = treatmentName;
    }

    public String getTimeslot() {
        return timeslot;
    }

    public void setTimeslot(String timeslot) {
        this.timeslot = timeslot;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getAppointmentStatus() {
        return appointmentStatus;
    }

    public void setAppointmentStatus(String appointmentStatus) {
        this.appointmentStatus = appointmentStatus;
    }
}
