public class AreaOfExpertise {
    private String exp;

    public AreaOfExpertise(String e) {
        this.setExp(e);
    }

    public AreaOfExpertise() {
        this.exp = "";
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public String toString() {
        return "Expertise" + this.exp;
    }
}
