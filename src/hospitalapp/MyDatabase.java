import java.lang.reflect.Array;
import java.util.ArrayList;

public class MyDatabase {
    static final int n = 3;
    public ArrayList<Physician> physiciansDB = new ArrayList<>();
    public ArrayList<Patient> patientDB = new ArrayList<>();
    public ArrayList<Timetable> timetableDB = new ArrayList<>();
    public ArrayList<AreaOfExpertise> areaOfExpertise = new ArrayList<>();
    public ArrayList<Appointment> appointmentDB = new ArrayList<>();
    // static AreaOfExpertise[] areaOfExpertise = new AreaOfExpertise[n];

    public ArrayList addPatient() {
        Patient p1 = new Patient("John", 1, 12345654);
        Patient p2 = new Patient("James", 2, 12345653);
        Patient p3 = new Patient("Joana", 3, 12345656);
        Patient p4 = new Patient("George", 4, 12345654);

        patientDB.add(p1);
        patientDB.add(p2);
        patientDB.add(p3);
        patientDB.add(p4);

        return patientDB;
    }

    public ArrayList addPhysician() {
        Physician phy1 = new Physician(1, "Nandana", "Hatfield", "Osteopathy", 1123245, "Monday 7:00-9:00");
        Physician phy2 = new Physician(2, "Hari", "Surrey", "Physiotherapy", 112324455, "Tuesday 7:00-9:00");
        Physician phy3 = new Physician(3, "Satish", "London", "Osteopathy", 1123247865, "Wednesday 7:00-9:00");
        Physician phy4 = new Physician(4, "Sai", "Edinburgh", "Rehabilitation", 11235245, "Thursday 7:00-9:00");
        Physician phy5 = new Physician(5, "Tatiana", "Hertfordshire", "Physiotherapy", 115675245, "Friday 7:00-9:00");
        // System.out.println("Helloooooo" + phy1);
        physiciansDB.add(phy1);
        physiciansDB.add(phy2);
        physiciansDB.add(phy3);
        physiciansDB.add(phy4);
        physiciansDB.add(phy5);
        // System.out.println(physiciansDB.get(2));
        return physiciansDB;
    }

    public ArrayList getPhysicians() {
        return physiciansDB;
    }

    public ArrayList addExpertise() {
        // Hardcoding values of Area of Expertise
        AreaOfExpertise osteopathy = new AreaOfExpertise("Osteopathy");
        AreaOfExpertise physiotherapy = new AreaOfExpertise("Physiotherapy");
        AreaOfExpertise rehabilitation = new AreaOfExpertise("Rehabilitation");

        // areaOfExpertise[0] = osteopathy;
        // areaOfExpertise[1] = physiotherapy;
        // areaOfExpertise[2] = rehabilitation;

        areaOfExpertise.add(osteopathy);
        areaOfExpertise.add(physiotherapy);
        areaOfExpertise.add(rehabilitation);

        return areaOfExpertise;
    }

    public ArrayList getExpertise() {
        return areaOfExpertise;
    }

    public ArrayList addTimetable() {
        Timetable t1 = new Timetable(1, "Treatment", "Hari", "Pool Rehabilitation", "01-05-2021 7:00-7:30",
                "Swimming Pool - A", "Available");

        Timetable t2 = new Timetable(2, "Treatment", "Tatiana", "Pool Rehabilitation", "02-05-2021 7:00-7:30",
                "Swimming Pool - A", "Available");

        Timetable t3 = new Timetable(3, "Hari", "Treatment", "Pool Rehabilitation", "03-05-2021 7:00-7:30", "Gym - A",
                "Available");

        Timetable t4 = new Timetable(4, "Hari", "Consultation", "Pool Rehabilitation", "03-05-2021 7:00-7:30",
                "Consultation Room", "Available");

        timetableDB.add(t1);
        timetableDB.add(t2);
        timetableDB.add(t3);
        timetableDB.add(t4);

        return timetableDB;
    }

    public ArrayList addAppointment() {
        Appointment a1 = new Appointment("John", 1);

        appointmentDB.add(a1);

        return appointmentDB;

    }
}
