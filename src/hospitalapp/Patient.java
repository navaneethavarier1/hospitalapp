public class Patient {
    private int uniqueId;
    private String patientName;
    private int telephone;

    public Patient(String name, int uniqueId, int telephone) {
        this.setName(name);
        this.setId(uniqueId);
        this.setPhoneNumber(telephone);
    }

    public Patient() {
        this.patientName = "";
        this.uniqueId = -1;
    }

    public String getName() {
        return this.patientName;
    }

    public void setName(String name) {
        this.patientName = name;
    }

    public int getId() {
        return this.uniqueId;
    }

    public void setId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    public int getPhoneNumber() {
        return this.telephone;
    }

    public void setPhoneNumber(int telephone) {
        this.telephone = telephone;
    }

    public String toString() {

        return "Patient Name: " + this.patientName + " Customer Phone Number " + this.telephone + " Customer unique Id "
                + this.uniqueId;
    }
}
